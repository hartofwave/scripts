#!/bin/bash
#Checks if 8.8.8.8 is ping-able, used to find how long my UPS will last at idle.
while true; do
  echo "IPv4 is up $(date)"
if ping -q -c 1 -W 1 8.8.8.8 >/dev/null; then
  sleep 5
  continue	
else
  echo "IPv4 is down $(date)"
  break	
fi
done
