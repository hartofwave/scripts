#!/usr/bin/env bash
#Final year project set up script, currently broken in parts, needs the requirements.txt for example
LFW_DIR="data/lfw/"
MODEL_DIR="20180402-114759/"
VENV_DIR="venv/lib/python3.*/site-packages/"
FACENET_DIR="facenet_master"

if [ -d "venv/" ]; then
    echo "Attaching to the venv"
    source venv/bin/activate
    echo "Installing requirements.txt"
    pip3 install -q -r requirements.txt
else
    echo "No such $VENV_DIR, is there a venv?"
    echo "Trying to make venv"
    python3 -m venv venv/
fi
#LFW data set download and check
if [ -d "$LFW_DIR" ]; then
    echo "Dir $LFW_DIR found, data set assumed to be present."
else
    if [ -d "downloads/lfw.tgz" ]; then
        mkdir data/lfw/
        tar xvf downloads/lfw.tgz -C data/
    else
        echo "No lfw.tgz, downloading LFW data set"
        wget -P downloads/ http://vis-www.cs.umass.edu/lfw/lfw.tgz
        mkdir data/lfw/
        tar xvf downloads/lfw.tgz -C data/
    fi
fi
#Model download and check
if [ -d "$MODEL_DIR" ]; then
    echo "Dir $MODEL_DIR found, model assumed to be present inside it."
else
    echo "$MODEL_DIR is Empty, downloading pre-trained model"
    wget --no-check-certificate -r 'https://drive.google.com/uc?export=download&confirm=S3qm&id=1EXPBSXwTaqrSC0OhUdXNmKSh9qJUQ55-' -O model.zip
    unzip model.zip
fi
if [ -d "$FACENET_DIR" ]; then
    echo "Dir $FACENET_DIR found, data set assumed to be present."
else
    echo "$FACENET_DIR is Empty, downloading facenet to facenet_master"
git clone https://github.com/HartOfWave/facenet.git facenet_master
fi
