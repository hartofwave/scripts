#!/bin/bash
#Takes the output of speedtest-cli and appends it to 'speedtest.log'
#Use in combination with cron
file="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/speedtest.log"
speedtestout=$(speedtest-cli --json)
echo "$file"
if [ ! -f $file ]; then
echo "file not found"
$touch $file
fi
echo "$speedtestout" >> "$file"
